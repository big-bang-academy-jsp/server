## Installation

```bash
$ npm install
```

## Environment

```bash
In ".env" file at root level
PORT - the server port number. Default as 8080
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
