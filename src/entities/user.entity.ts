import { UserProps } from 'src/types';

export class User implements UserProps {
  id: number;
  name: string;
}
