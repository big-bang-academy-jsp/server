import {
  Injectable,
  // Logger
} from '@nestjs/common';
import { readFile } from 'fs/promises';
import path from 'path';
import { User } from '../entities/user.entity';

export interface UserList {
  users: User[];
}

@Injectable()
export class UserService {
  // private logger = new Logger(UserService.name);

  async list(): Promise<UserList> {
    const jsonPath = '../assets/data.json';
    const buffer = await readFile(path.resolve(__dirname, jsonPath));
    const data = JSON.parse(buffer.toString());
    // this.logger.debug('data');
    // this.logger.debug(data);
    return data;
  }
}
